#! /usr/bin/python

# To change this license header, choose License Headers in Project Properties.
# To change this template file, choose Tools | Templates
# and open the template in the editor.

__author__ = "aanand"
__date__ = "$Mar 23, 2015 5:43:14 PM$"

import os
import newspaper
import codecs
import json
import sys
import csv

from newspaper import Article
from os import path
def getFileList(path, mapdirectory, outDirectory, separator):
    print("Start getFileList")
    mapFiles = os.listdir(mapdirectory)
    d = {}
    d1 ={}
    
    for root, dirs, files in os.walk(path):
        data = {}
        json_data = json.dumps(data)
        if not len(files) == 0:
            day = ""
            for file in files:
                day = file[:file.find("-")]
                print("Day : "+day)
                for mapFile in mapFiles:        
                    if day == mapFile[:mapFile.find(".")]:
                        f1 = codecs.open(mapdirectory+separator+mapFile, "r", "utf-8")
                        for line in f1:
                            idPath = line.split("\t")[-2]
                            id = idPath[idPath.find("-")+1:idPath.rfind("-")]
                            d[id] = line.split("\t")[-3]
                            d1[id] = line
                if not os.path.exists(outDirectory):
                    os.makedirs(outDirectory)                    
                outputFile = outDirectory+separator+day+".json"
                f = codecs.open(outputFile, "a", "utf-8")
                break
            for file in files:
                seq=""
                url = ""
                metadata = ""
                if file.endswith(".html"):
                    try:
                        a = Article(root+separator+file, language='en')
                        a.download()
                        a.parse()                   
                        if not '403 Forbidden' in a.title and not '404 - Page not found' in a.text and not '404 Not Found'in a.title and not '404 - Page Cannot Be Found'in a.title and not '404 - Not Found'in a.text and not '404 error'in a.text and not 'Page Not Found (404)'in a.text and not 'Error: 404'in a.title and not '404 error'in a.title and not 'Page Not Found' in a.title and not '301 Moved Permanently' in a.title and not 'Page Unavailable' in a.title and not 'Error 404' in a.title and not '404 Not Found' in a.text and not 'page you requested could not be found' in a.text and not a.text == "" and not  a.title == "":
                            seq = file[file.find("-")+1:file.rfind("-")]
                            if seq in d:
                                url = d.get(seq)
                                metadata = d1.get(seq)
                            data['URL'] = url
                            data['LocalFile'] = root+separator+file
                            data['Sequence'] = seq
                            data['Title'] = a.title
                            data['Content'] = a.text
                            data['GdeltMetaData'] = metadata
                            json.dump(data,f, indent=4)
                    except: 
                        pass
            f.close()
    print("End getFileList")
if __name__ == "__main__":
#    inputFileName = sys.argv[1]
#    path ="C:\\Users\\aanand\\Downloads\\20140101\\GW\\D5data-1\\vsetty\\data.gdeltproject.org\\finished-crawls"
#    mapdirectory = 'C:\\Users\\aanand\\Downloads\\20140101\\GW\\D5data-1\\vsetty\\data.gdeltproject.org\\finished-crawls\\MapFile'
#    outDirectory = 'C:\\Users\\aanand\\Downloads\\20140101\\GW\\D5data-1\\vsetty\\data.gdeltproject.org\\finished-crawls\\outFolder'
    
    path = sys.argv[1]
    outDirectory = sys.argv[2]
    mapdirectory = sys.argv[3]
    separator = "\\"
    getFileList(path, mapdirectory,outDirectory, separator )
#    data = {}
#    json_data = json.dumps(data)
#    for dir1 in os.listdir(path):
#        if  os.path.isdir(path+ separator+dir1):
#            print 'blah'+ path+ separator+dir1
#        day = ""
#        sequence =""
#        folderPath = path+ separator+dir1
#        print folderPath
#        day = dir1
#        outputFile = ""
#        for file in os.listdir(folderPath): 
#            folderP = folderPath+separator+file
#            if os.path.isdir(folderP):
#                folderPath = folderP
#                print folderPath
#                break
#            print file
#        if not os.path.exists(outDirectory):
#                os.makedirs(outDirectory)
#        outputFile = outDirectory+separator+day+".json"
#        f = codecs.open(outputFile, "a", "utf-8")
##            if file.endswith(".html"):
##                path1 = folderPath+separator+file
##        #        print path1
##                k = path1.rfind(separator)
##                path1 = path1[k:path1.rfind("-")]
##                day = path1[1:path1.find("-")]
##                sequence = path1[path1.find("-")+1:]
##                output = outDirectory+separator
##        #        print output
##                if not os.path.exists(outDirectory):
##                    os.makedirs(outDirectory)
##                outputFile = output+day+".json"      
##                f = codecs.open(outputFile, "a", "utf-8")
##                break
#            
#        mapFiles = os.listdir(mapdirectory)
#        d = {}
#        d1 ={}
#        for mapFile in mapFiles:        
#            print day
#            if day == mapFile[:mapFile.find(".")]:
#                f1 = codecs.open(mapdirectory+separator+mapFile, "r", "utf-8")
#                for line in f1:
#                    idPath = line.split("\t")[-2]
#                    id = idPath[idPath.find("-")+1:idPath.rfind("-")]
#                    d[id] = line.split("\t")[-3]
#                    d1[id] = line
#        print folderPath
#        for file in os.listdir(folderPath): 
#            seq=""
#            url = ""
#            metadata = ""
#            if file.endswith(".html"):
##                try:
#                    a = Article(folderPath+separator+file, language='en')
#                    a.download()
#                    a.parse()                   
#                    if not '403 Forbidden' in a.title and not 'Page Not Found' in a.title and not '301 Moved Permanently' in a.title and not 'Page Unavailable' in a.title and not 'Error 404' in a.title and not '404 Not Found' in a.text and not 'page you requested could not be found' in a.text and not a.text == "" and not  a.title == "":
#                        seq = file[file.find("-")+1:file.rfind("-")]
#                        if seq in d:
#                            url = d.get(seq)
#                            metadata = d1.get(seq)
#                        data['URL'] = url
#                        data['LocalFile'] = folderPath+separator+file
#                        data['Sequence'] = seq
#                        data['Title'] = a.title
#                        data['Content'] = a.text
#                        data['GdeltMetaData'] = metadata
#                        json.dump(data,f, indent=4)
##                except Exception: 
##                    pass
#    print "done"
#    f.close()
    
